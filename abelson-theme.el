;;; abelson-theme.el --- A slightly warmer version of the default theme

;; Author: Brian Gomes Bascoy
;; URL: https://gitlab.com/peramides/abelson-theme

(deftheme abelson
  "A slightly warmer version of the default theme.")

(custom-theme-set-faces
 'abelson
 '(default ((t (:foreground "black" :background "Snow"))))
 '(region ((t (:background "gainsboro"))))
 '(hl-line ((t :background "OldLace")))
 '(line-number ((t :background "grey95")))
 '(line-number-current-line ((t :background "gold" :foreground "black")))
 '(fringe ((((class color) (background light)) (:background "grey95"))))
 '(olivetti-fringe ((((class color) (background light)) (:background "Snow")))))

(provide-theme 'abelson)
